package main;

import com.welcome.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) {

        Hello hello = new Hello();

        try {

            String name = readName();

            hello.setupName(name);

        }catch (Exception e){

            System.out.println(e.getMessage());

        }

        hello.welcome();

        System.out.println("Hello, World!");

        hello.byeBye();

    }

    public static String readName() throws IOException{

        System.out.println("Please, enter your name!");

        String name = reader.readLine();

        while (name.isEmpty()) {

            System.out.println("Please, enter your name!");

            name = reader.readLine();
        }

        return name;
    }
}
