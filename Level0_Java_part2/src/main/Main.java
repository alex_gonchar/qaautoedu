package main;

import games.*;
import calculator.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) {

        try {

            mainMenu();

        }catch (Exception e){

            System.out.println("Shit happens...");

        }
    }

    public static void buildMatrix(){

        try {

            System.out.println("Please, enter value from 1 to 9. " +
                    "(If you want to finish the program, enter \"Exit\" and press Enter)");

            lab1: while (true) {

                String s = reader.readLine();

                if (matchWithRegx(s)) {

                    int value = Integer.parseInt(s);

                    int[][] arr = Matrix.matrix(value);

                    matrixToScreen(arr);

                    while (true){

                        System.out.println("Do you want to continue? Y/N" );

                        s = reader.readLine();

                        if(s.equalsIgnoreCase("Y")){

                            System.out.println("Please, enter value from 1 to 9. " +
                                    "(If you want to finish the program, enter \"Exit\" and press Enter)");

                            continue lab1;

                        } else
                        if (s.equalsIgnoreCase("N")) break;
                    }

                    System.out.println("Good bye!");

                    break;

                } else {

                    if (s.equalsIgnoreCase("exit")){

                        System.out.println("Good bye!");

                        System.exit(1);
                    }

                    System.err.println("Value is incorrect. Please, enter value from 1 to 9. " +
                            "(If you want to finish the program, enter \"Exit\" and press Enter)");

                    continue;
                }
            }
        } catch (IOException e){

            System.err.println(e.getMessage());
        }

    }

    public static void matrixToScreen(int[][] arr){

        for(int []i : arr){

            for(int j : i){

                System.out.print(j + " ");
            }

            System.out.println();
        }
    }

    public static boolean matchWithRegx(String str){

        Pattern p = Pattern.compile("^(1|2|3|4|5|6|7|8|9)$");

        Matcher m = p.matcher(str);

        return m.matches();
    }
    public static void menu1(){
        System.out.println("==========================");
        System.out.println("Please, choose that you want to do");
        System.out.println("1 - Test Matrix");
        System.out.println("2 - Test Arithmetic");
        System.out.println("3 - Test Snail");
        System.out.println("4 - Test Palindrom");
        System.out.println("5 - Exit");
        System.out.println("==========================");
    }

    public static void menu2(){
        System.out.println("==========================");
        System.out.println("Please, choose that you want to do");
        System.out.println("1 - Continue");
        System.out.println("2 - Exit");
        System.out.println("==========================");
    }

    public static void menu3(){
        System.out.println("==========================");
        System.out.println("Please, choose that you want to do");
        System.out.println("1 - Test Multiplication Array method");
        System.out.println("2 - Test Power Method");
        System.out.println("3 - Test Division method");
        System.out.println("4 - Test Root method");
        System.out.println("5 - Exit");
        System.out.println("==========================");
    }

    public static void menu4(){
        System.out.println("==========================");
        System.out.println("Please, choose that you want to do");
        System.out.println("1 - Test palindrom word");
        System.out.println("2 - Test palindrom phrase");
        System.out.println("==========================");
    }

    public static void mainMenu()throws Exception{

        while (true){

            menu1();

            String s = reader.readLine();

            if (s.equalsIgnoreCase("5")) System.exit(1);

            while (!matchWithRegx(s)){

                System.err.println("Incorrect value!");

                s = reader.readLine();
            }

            int value = Integer.parseInt(s);

            switch (value){
                case 1: buildMatrix();
                    break;
                case 2: calculatorMenu();
                    break;
                case 3: snailMenu();
                    break;
                case 4: palindromMenu();
                    break;
            }

            menu2();

            s = reader.readLine();

            while (!matchWithRegx(s)){

                System.err.println("Incorrect value!");

                s = reader.readLine();
            }

            int num = Integer.parseInt(s);

            while (!(num == 1 || num == 2)){

                System.err.println("Out of range! Please choose 1 or 2!");

                s = reader.readLine();

                num = Integer.parseInt(s);
            }

            if (num == 1) continue;

            if (num == 2) break;

        }

    }

    public static void calculatorMenu() throws Exception{

        menu3();

        String s = reader.readLine();

        while (true){

            if (s.equalsIgnoreCase("5")) System.exit(1);

            while (!matchWithRegx(s)){

                System.err.println("Incorrect value!");

                s = reader.readLine();
            }

            int value = Integer.parseInt(s);

            switch (value){
                case 1: System.out.println("Resault of multiplication operation is " + Arithmetic.arrayMultiplication(readArrFromConsole()));
                    break;
                case 2: System.out.println("Resault of power(a, b)-operation is " + Arithmetic.power(readNumberFromConsole(), readPowerFromConsole()));
                    break;
                case 3: System.out.println("Resault of division operation is " + Arithmetic.division(readNumberFromConsole(), readPowerFromConsole()));
                    break;
                case 4: Arithmetic.root(readNumberFromConsole());
                    break;
            }

            menu2();

            s = reader.readLine();

            while (!matchWithRegx(s)){

                System.err.println("Incorrect value!");

                s = reader.readLine();
            }

            int num = Integer.parseInt(s);

            while (!(num == 1 || num == 2)){

                System.err.println("Out of range! Please choose 1 or 2!");

                s = reader.readLine();

                num = Integer.parseInt(s);
            }

            if (num == 1) mainMenu();

            if (num == 2) System.exit(1);

        }

    }

    public static ArrayList<Double> readArrFromConsole() throws Exception{

        ArrayList<Double> list = new ArrayList<Double>();

        System.out.println("Enter the values via enter. To finish input, type \"End\" and press enter");

        while (true){

            try {

                String s = reader.readLine();

                if (s.equalsIgnoreCase("end")) break;

                list.add(Double.parseDouble(s));

            }catch (NumberFormatException e){

                System.err.println("It is not a number! Please, try your input...");

                list.clear();

                continue;
            }
        }

        return list;
    }

    public static double readNumberFromConsole() throws Exception{

        double value = 0.0;

        System.out.println("Enter the number that you want to bring to the power. If you want to exit enter \"End\" and press Enter");

        while (true){

            try {

                String s = reader.readLine();

                if (s.equalsIgnoreCase("end")) System.exit(1);

                value = Double.parseDouble(s);

                break;

            }catch (NumberFormatException e){

                System.err.println("It is not a number! Please, try your input...");

                continue;
            }
        }
        return value;
    }

    public static double readPowerFromConsole() throws Exception{

        double value = 0.0;

        System.out.println("Enter the power of previous number. If you want to exit enter \"End\" and press Enter");

        while (true){

            try {

                String s = reader.readLine();

                if (s.equalsIgnoreCase("end")) System.exit(1);

                value = Double.parseDouble(s);

                break;

            }catch (NumberFormatException e){

                System.err.println("It is not a number! Please, try your input...");

                continue;
            }
        }
        return value;
    }

    public static void snailMenu() throws Exception{

        System.out.println("Enter number from 3 to 31. If you want to exit enter \"End\" and press Enter");

        int value = 0;

        while (true){

            try {
                String s = reader.readLine();

                if (s.equalsIgnoreCase("end")) break;

                value = Integer.parseInt(s);

                if (value >= 3 && value <= 31){

                    Snail.printSnail(value, Snail.calculateSnail(value));

                    break;

                } else {

                    System.err.println("The number is out of range (3...31). Please, try your input...");

                }

            }catch (NumberFormatException e){

                System.err.println("It is not a number! Please, try your input...");

                continue;
            }
        }
    }

    public static void palindromMenu() throws Exception {

        menu4();

        int value = 0;

        while (true){

            try {
                String s = reader.readLine();

                //if (s.equalsIgnoreCase("3")) break;

                value = Integer.parseInt(s);

                if (value == 1) {

                    System.out.println("Please, enter a word...");

                    System.out.println("Is this word palindrom: " + Palindrom.checkWord(reader.readLine()));

                    break;

                } else if (value == 2){

                    System.out.println("Please, enter the a phrase...");

                    System.out.println("Is this phrase palindrom: " + Palindrom.checkPhrase(reader.readLine()));

                    break;

                } else {

                    System.err.println("You must enter 1 or 2. Is it difficult?");

                    continue;
                }

            } catch (NumberFormatException e){

                System.err.println("Please, enter a number!");

                continue;
            }
        }
    }
}
