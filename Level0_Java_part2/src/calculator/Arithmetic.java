package calculator;

import java.util.ArrayList;

public class Arithmetic {

    public static double arrayMultiplication(ArrayList<Double> list){

        double result = 1.0;

        for (int i = 0; i < list.size(); i++){

            result *= list.get(i);
        }

        return result;
    }

    public static double power (double a, double b){

        return Math.pow(a, b);
    }

    public static double division (double a, double b){

        return a / b;

    }

    public static double root (double a){

        return Math.sqrt(a);

    }

}
