package games;

public class Palindrom {

    public static boolean checkWord(String someWord){

        return someWord.equalsIgnoreCase((new StringBuilder(someWord)).reverse().toString());
    }

    public static boolean checkPhrase(String somePhrase){

        somePhrase = somePhrase.replace(" ", "");

        return checkWord(somePhrase);
    }
}
