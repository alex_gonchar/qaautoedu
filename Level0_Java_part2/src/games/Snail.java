package games;

public class Snail {

    public static int[][] calculateSnail(int size){

        int [][] arr = new int[size][size];

        int count = size * size;

        int step = 1;

        int temp = 0;

        for (int i = 0; i < size; i++){

            for (int j = size - step; j >= temp; j--){

                if (count == 0) break;

                arr[i][j] = count;

                count--;

            }

            if (count == 0) break;

            for (int k = step; k < size - temp; k++){

                if (count == 0) break;

                arr[k][i] = count;

                count--;
            }

            if (count == 0) break;

            for (int m = step; m < size - temp; m++){

                if (count == 0) break;

                arr[size - step][m] = count;

                count--;
            }

            if (count == 0) break;

            for (int n = size - step - 1; n >= step; n--){

                if (count == 0) break;

                arr[n][size-step] = count;

                count--;
            }

            if (count == 0) break;

            step++;

            temp++;

        }

        return arr;
    }

    public static void printSnail(int snailsize, int [][] arr) {

        for (int[] k : arr) {

            for (int m : k) {

                if ((m / 10) == 0) {

                    System.out.print(m + "   ");

                } else if (m / 100 == 0) {

                    System.out.print(m + "  ");

                } else {

                    System.out.print(m + " ");
                }
            }
            System.out.println();
        }
    }
}

