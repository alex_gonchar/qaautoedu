package games;

public class Matrix {

    public static int[][] matrix(int num){

        int[][] arr = new int[num][num];

        int a = 1;

        for (int i = 0; i < arr.length; i++){

            for (int j = 0; j < arr[i].length; j++){

                arr[i][j] = a;

                a++;

                if (a >= 10) a = 1;
            }
        }
        return arr;
    }
}
