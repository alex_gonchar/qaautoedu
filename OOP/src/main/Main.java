package main;

import figure.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    static int value;

    static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) {

        try {

            Shape[] arr = randShape();

            while (true) {

                menu();

                switch (value) {

                    case 1:
                        System.out.println("Изначальный массив рандомных фигур");
                        printArr(arr);
                        System.out.println();
                        System.out.println("Массив фигур с изменненной позицией");
                        changePositionShape(arr);
                        printArr(arr);
                        System.out.println();
                        break;
                    case 2:
                        System.out.println("Изначальный массив рандомных фигур");
                        printArr(arr);
                        System.out.println();
                        System.out.println("Массив фигур с изменненным размером");
                        changeSizeShape(arr);
                        printArr(arr);
                        System.out.println();
                        break;
                    case 3:
                        System.out.println("Изначальный массив рандомных фигур");
                        printArr(arr);
                        System.out.println();
                        System.out.println("Отсортированный массив фигур по площади");
                        sortArr(arr);
                        printArr(arr);
                        System.out.println();
                        break;
                }

            }

        } catch (IOException e) {
            System.err.println("Что-то пошло не так...");
        }
    }

    public static void menu() throws IOException {

        System.out.println("|      Что вы хотите сделать с массивом рандомных фигур?                       |");
        System.out.printf("================================================================================%n");
        System.out.println("|  1 - Переместить все фигуры в массиве используя рандомный коэффициент        |");
        System.out.println("|  2 - Изменить размер всех фигур в массиве используя рандомный коэффициент    |");
        System.out.println("|  3 - Сортировка фирур в массиве по площади                                   |");
        System.out.println("|  0 - Завершить работу программы                                              |");
        System.out.printf("================================================================================%n");

        String s = reader.readLine();

        while (!matchInput(s)) {
            System.out.println("Некорректное значение. Повторите попытку. Чтобы завершить работу введите Exit и нажмите энтр.");
            s = reader.readLine();

            if (s.equalsIgnoreCase("exit")) System.exit(1);
        }

        value = Integer.parseInt(s);

        if (value == 0) System.exit(1);
    }


    public static Shape[] randShape() {
        Shape[] arr = new Shape[10];
        for (int i = 0; i < arr.length; i++) {
            switch (1 + new Random().nextInt(3)) {

                case 1:
                    arr[i] = new Circle();
                    break;
                case 2:
                    arr[i] = new Square();
                    break;
                case 3:
                    arr[i] = new Triangle();
            }
        }
        return arr;
    }

    public static void printArr(Shape[] arr) {
        for (Shape shape : arr) {
            System.out.println(shape);
        }
    }

    public static void changeSizeShape(Shape[] arr) {
        for (int i = 0; i < arr.length; i++) {
            arr[i].changeSize(Math.random());
        }
    }

    public static void sortArr(Shape[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            boolean swapped = false;
            for (int j = 0; j < arr.length - i - 1; j++) {
                if (arr[j].compareTo(arr[j + 1]) > 0) {
                    Shape tmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = tmp;
                    swapped = true;
                }
            }
            if (!swapped)
                break;
        }
    }

    public static void changePositionShape(Shape[] arr) {
        for (int i = 0; i < arr.length; i++) {
            arr[i].changePosition((double) new Random().nextInt(201) - 100, (double) new Random().nextInt(201) - 100);
        }
    }

    public static boolean matchInput(String s) {
        Pattern p = Pattern.compile("^(0|1|2|3)$");
        Matcher m = p.matcher(s);
        return m.matches();
    }
}
