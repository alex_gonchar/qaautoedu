package figure;

import java.util.Random;

public class Triangle extends Shape {

    private double defX, defY;

    private double secondpointX = 0.1 + (new Random().nextDouble() * (100.0 - 0.1));
    private double secondpointY = 0.1 + (new Random().nextDouble() * (100.0 - 0.1));
    private double thirdpointX = 0.1 + (new Random().nextDouble() * (100.0 - 0.1));
    private double thirdpointY = 0.1 + (new Random().nextDouble() * (100.0 - 0.1));


    @Override
    public void changePosition(double x, double y){
        this.defX += x;
        this.defY += y;
        this.secondpointX += x;
        this.secondpointY += y;
        this.thirdpointX += x;
        this.thirdpointY += y;
    }

    @Override
    public double calculateSquare(){
        double ab = Math.sqrt((secondpointX - defX) * (secondpointX - defX) + (secondpointY - defY)*(secondpointY - defY));
        double bc = Math.sqrt((thirdpointX - secondpointX) * (thirdpointX - secondpointX) + (thirdpointY - secondpointY)*(thirdpointY - secondpointY));
        double ac = Math.sqrt((thirdpointX- defX) * (thirdpointX - defX) + (thirdpointY - defY)*(thirdpointY - defY));
        double p = (ab + bc + ac)/2;
        double square = Math.sqrt(p * (p - ab) * (p - bc) * (p - ac));
        return square;
    }

    @Override
    public void changeSize(double koef){

        this.secondpointX *= koef;
        this.secondpointY *= koef;
        this.thirdpointX *= koef;
        this.thirdpointY *= koef;
    }

    @Override
    public String toString() {
        return "Triangle: т.А(" + defX + ", " + defY + ") | " + " т.В(" + secondpointX + ", " + secondpointY + ") | " + " т.C("
                + thirdpointX + ", " + thirdpointY + ") | " + "S(triangle) = " +
                + calculateSquare();
    }
}
