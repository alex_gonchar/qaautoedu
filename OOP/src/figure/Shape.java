package figure;

public abstract class Shape implements Comparable<Shape> {

    public abstract void changePosition(double X, double Y);

    public abstract void changeSize(double koef);

    public abstract double calculateSquare();

    @Override
    public int compareTo(Shape o) {

        if (this.calculateSquare() < o.calculateSquare()) return -1;
        else if (this.calculateSquare() > o.calculateSquare()) return 1;
        else return 0;
    }
}
