package figure;

import java.util.Random;

public class Square extends Shape {

    private double defX, defY;
    private double secondPointX = 0.1 + (new Random().nextDouble() * (100.0 - 0.1));
    private double secondPointY = 0.1 + (new Random().nextDouble() * (100.0 - 0.1));
    private double square;

    @Override
    public void changePosition(double x, double y) {
        this.defX += x;
        this.defY += y;
        this.secondPointX += x;
        this.secondPointY += y;
    }

    @Override
    public double calculateSquare() {

        square = Math.abs((secondPointX - defX) * (secondPointY - defY));
        return square;
    }

    @Override
    public void changeSize(double koef) {

        this.secondPointX *= koef;
        this.secondPointY *= koef;
    }

    @Override
    public String toString() {
        return "Square:   т.А(" + defX + ", " + defY + ") | " + " т.В(" + secondPointX + ", " + secondPointY + ") | " + "S(square) = " +
                +calculateSquare();
    }
}
