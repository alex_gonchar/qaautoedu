package figure;

import java.util.Random;

public class Circle extends Shape {

    private double defX, defY;

    private double radiusX = 0.1 + (new Random().nextDouble() * (100.0 - 0.1));
    private double radiusY = 0.1 + (new Random().nextDouble() * (100.0 - 0.1));

    @Override
    public void changePosition(double x, double y) {
        this.defX += x;
        this.defY += y;
        this.radiusX += x;
        this.radiusY += y;
    }

    @Override
    public double calculateSquare() {
        double radiuslenght = Math.sqrt((radiusX - defX) * (radiusX - defX) + (radiusY - defY) * ((radiusY - defY)));
        double square = 2 * Math.PI * radiuslenght * radiuslenght;
        return square;
    }

    @Override
    public void changeSize(double koef) {
        this.radiusX *= koef;
        this.radiusY *= koef;
    }

    @Override
    public String toString() {
        return "Circle:   т.А(" + defX + ", " + defY + ") | " + " т.В(" + radiusX + ", " + radiusY + ") | " + "S(circle) = " +
                +calculateSquare();
    }
}
